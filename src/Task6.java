public class Task6 {

	private int min = (int) Double.POSITIVE_INFINITY;
	private int max = 0;

	public Task6() {}

	public Task6(int val) {
		setValues(val);
	}

	public Task6(int val_1, int val_2) {
		setValues(val_1, val_2);
	}

	public void showValues() {
		System.out.println("min: " + min);
		System.out.println("max: " + max);
	}
	public void setValues(int val) {
		if(val < this.min) this.min = val;
		if(val > this.max) this.max = val;
	}
	public void setValues(int val_1, int val_2) {
		setValues(val_1);
		setValues(val_2);
	}
}
