public class Task4 {

	private char symbol;
	private int number;

	public Task4(char symbol, int number) {
		this.symbol = symbol;
		this.number = number;
	}

	public Task4(double config) {
		String s = String.valueOf(config);
		this.symbol = (char) (int) config;
		this.number = Integer.parseInt(s.substring(s.lastIndexOf(".") + 1, s.lastIndexOf(".") + 3));
	}

}
