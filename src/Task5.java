public class Task5 {

	private int value;

	public Task5(int value) {
		setValue(value);
	}

	public Task5() {
		setValue();
	}

	public void showValue() {
		System.out.println(value);
	}
	public void setValue(int value) {
		this.value = (value <= 100) ? value : 100;
	}
	public void setValue() {
		this.value = 0;
	}

}
