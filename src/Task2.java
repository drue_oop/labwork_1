public class Task2 {

	private char val_1;
	private char val_2;

	public Task2(char val_1, char val_2) {
		this.val_1 = val_1;
		this.val_2 = val_2;
	}

	public void getArrSymbols() {
		for (int i = val_1; i <= val_2; i++) System.out.println((char) i);
	}

}
