public class Task3 {

	private int val_1;
	private int val_2;

	public Task3() {
		this.val_1 = this.val_2 = 0;
	}

	public Task3(int val) {
		this.val_1 = this.val_2 = val;
	}

	public Task3(int val_1, int val_2) {
		this.val_1 = val_1;
		this.val_2 = val_2;
	}

}
